package ch.cern.nile;

import ch.cern.nile.common.schema.SchemaInjector;
import ch.cern.nile.generator.ConnectSchemaToTableCreationCommandGenerator;
import ch.cern.nile.generator.DbType;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.OracleContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import org.testcontainers.utility.DockerImageName;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.Duration;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OraclePipelineTestIT {
    private static final Logger LOGGER = LoggerFactory.getLogger(OraclePipelineTestIT.class);

    private static final String CONNECTOR_NAME = "jdbc-source-connector";
    private static final String TOPIC_NAME = "source-topic";
    private static final String TEST_TABLE = "TEST_TABLE";
    private static final int ORACLE_TIMEOUT = 10000;
    private static final int CONSUMER_TIMEOUT = 1000;
    private static final String CP_KAFKA_VERSION = "6.2.1";

    private static String jdbcUrl;
    private static Network network;
    private static OracleContainer oracle;
    private static KafkaContainer kafka;
    private static GenericContainer<?> connect;

    private static final Map<String, Object> data = new HashMap<>();
    private static final Map<String, Object> dataWithDifferentType = new HashMap<>();

    @BeforeAll
    public static void initializeTestData() {
        data.put("byte_col", (byte) 1);
        data.put("short_col", (short) 2);
        data.put("int_col", 3);
        data.put("long_col", (long) 4);
        data.put("float_col", 5.0f);
        data.put("double_col", 6.0);
        data.put("boolean_col", true);
        data.put("string_col", "test");
        data.put("timestamp_col", 1501834166000L);
        data.put("bytes_col", new byte[]{1, 2, 3});
        dataWithDifferentType.put("int_col", 6.5);
    }


    @BeforeAll
    public static void initializeTestEnvironment() {
        network = Network.newNetwork();
        startOracleContainer();
        startKafkaContainer();
        startKafkaConnectContainer();
        initializeKafkaConnectJdbcConnector();
    }

    @AfterAll
    public static void cleanUpTestEnvironment() {
        stopContainers(connect, kafka, oracle);
    }

    private static void startOracleContainer() {
        oracle = new OracleContainer("gvenzl/oracle-xe:latest-faststart")
                .withNetwork(network)
                .withNetworkAliases("oracle");
        oracle.start();
        jdbcUrl = String.format("jdbc:oracle:thin:@oracle:%d/%s", 1521, oracle.getDatabaseName());
    }

    private static void startKafkaContainer() {
        final DockerImageName imageName = DockerImageName.parse(String.format("confluentinc/cp-kafka:%s", CP_KAFKA_VERSION));
        kafka = new KafkaContainer(imageName)
                .withNetwork(network)
                .withNetworkAliases("kafka")
                .withExposedPorts(9092, 9093)
                .waitingFor(Wait.forLogMessage(".*Kafka version.*", 1));
        kafka.start();
        LOGGER.info("Started Kafka Container. Bootstrap servers: {}", kafka.getBootstrapServers());
    }

    private static void startKafkaConnectContainer() {
        final DockerImageName imageName = DockerImageName.parse(String.format("confluentinc/cp-kafka-connect:%s", CP_KAFKA_VERSION));
        final Path connectorPath = Paths.get("src/test/resources/connectors");
        connect = new GenericContainer<>(imageName)
                .withNetwork(network)
                .withNetworkAliases("kafka-connect")
                .dependsOn(kafka, oracle)
                .withEnv("CONNECT_BOOTSTRAP_SERVERS", "kafka:9092")
                .withEnv("CONNECT_REST_ADVERTISED_HOST_NAME", "kafka-connect")
                .withEnv("CONNECT_REST_PORT", "28082")
                .withEnv("CONNECT_GROUP_ID", "quickstart")
                .withEnv("CONNECT_CONFIG_STORAGE_TOPIC", "connect-configs")
                .withEnv("CONNECT_OFFSET_STORAGE_TOPIC", "connect-offsets")
                .withEnv("CONNECT_STATUS_STORAGE_TOPIC", "connect-status")
                .withEnv("SQL_QUOTE_IDENTIFIERS_CONFIG", "false")
                .withEnv("CONNECT_KEY_CONVERTER", "org.apache.kafka.connect.json.JsonConverter")
                .withEnv("CONNECT_VALUE_CONVERTER", "org.apache.kafka.connect.json.JsonConverter")
                .withEnv("CONNECT_VALUE_CONVERTER_SCHEMAS_ENABLE", "true")
                .withEnv("CONNECT_PLUGIN_PATH", "/usr/share/java/kafka")
                .withEnv("KAFKA_JMX_OPTS",
                        "-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.rmi.port=1099 -Djava.rmi.server.hostname=localhost")
                .withFileSystemBind(connectorPath.toString(), "/usr/share/java/kafka/jdbc-connector")
                .withExposedPorts(28082, 1099)
                .waitingFor(Wait.forLogMessage(".*Kafka Connect started.*", 1));


        KafkaTopicCreator.createTopic(kafka.getBootstrapServers(), "connect-offsets", 1, (short) 1);
        KafkaTopicCreator.createTopic(kafka.getBootstrapServers(), "connect-configs", 1, (short) 1);
        KafkaTopicCreator.createTopic(kafka.getBootstrapServers(), "connect-status", 1, (short) 1);

        connect.start();

        LOGGER.info("Kafka Connect Started. Connection URL: {}", connect.getHost() + ":" + connect.getMappedPort(28082));
    }


    private static void initializeKafkaConnectJdbcConnector() {
        final int mappedPort = connect.getMappedPort(28082);
        final String connectUrl = "http://" + connect.getHost() + ":" + mappedPort + "/connectors";

        final Map<String, String> config = new HashMap<>();
        config.put("connector.class", "io.aiven.connect.jdbc.JdbcSinkConnector");
        config.put("table.name.format", TEST_TABLE);
        config.put("connection.password", oracle.getPassword());
        config.put("tasks.max", "1");
        config.put("auto.create", "false");
        config.put("auto.evolve", "false");
        config.put("topics", TOPIC_NAME);
        config.put("connection.user", oracle.getUsername());
        config.put("sql.quote.identifiers", "false");
        config.put("value.converter.schemas.enable", "true");
        config.put("connection.url", jdbcUrl);
        config.put("value.converter", "org.apache.kafka.connect.json.JsonConverter");
        config.put("key.converter", "org.apache.kafka.connect.storage.StringConverter");

        final Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("name", CONNECTOR_NAME);
        requestBody.put("config", config);

        final ObjectMapper objectMapper = new ObjectMapper();

        final int retries = 10;
        int sleepTimeMillis = 100;
        final int maxSleepTimeMillis = 1000;

        for (int i = 0; i < retries; i++) {
            try {
                final String json = objectMapper.writeValueAsString(requestBody);
                final HttpClient httpClient = HttpClient.newHttpClient();
                final HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create(connectUrl))
                        .header("Content-Type", "application/json")
                        .POST(HttpRequest.BodyPublishers.ofString(json))
                        .build();

                LOGGER.info("Sending request to create Kafka Connect connector: {}", request);
                final HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
                LOGGER.info("Received response: {}", response);

                if (response.statusCode() == 201) {
                    LOGGER.info("Connector created successfully");
                    return;
                } else {
                    LOGGER.error("Failed to create the connector, response code: {}, response body: {}", response.statusCode(), response.body());
                }
            } catch (Exception e) {
                LOGGER.error("Attempt {}/{} to create connector failed", i + 1, retries, e);
            }

            if (i < retries - 1) {
                try {
                    LOGGER.info("Waiting for {} ms before retrying to create Kafka Connect connector", sleepTimeMillis);
                    Thread.sleep(sleepTimeMillis);
                    sleepTimeMillis = Math.min(sleepTimeMillis * 2, maxSleepTimeMillis);
                } catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                    throw new RuntimeException("Thread interrupted during sleep between retries", ie);
                }
            }
        }

        final String logs = connect.getLogs();
        LOGGER.error("Final attempt failed. Kafka Connect logs: {}", logs);
        throw new RuntimeException("Failed to create the connector after " + retries + " retries");
    }

    private static void stopContainers(GenericContainer<?>... containers) {
        for (GenericContainer<?> container : containers) {
            if (container != null) {
                container.stop();
            }
        }
    }

    private void produceRecord(final String message) {
        final Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafka.getBootstrapServers());
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        final KafkaProducer<String, String> producer = new KafkaProducer<>(properties);

        final ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC_NAME, message);

        producer.send(record, (metadata, exception) -> {
            LOGGER.info("Trying to write to topic " + record.topic());
            if (exception != null) {
                LOGGER.error("Error producing to topic", exception);
                fail("Failed to produce message");
            } else {
                LOGGER.info("Message produced successfully");
                LOGGER.info(String.valueOf(metadata.offset()));
            }
        });
        producer.flush();
        producer.close();
    }

    private boolean hasMessagesInTopic() {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafka.getBootstrapServers());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "test-group");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");

        final Consumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(TOPIC_NAME));

        final ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(CONSUMER_TIMEOUT));
        consumer.close();

        return !records.isEmpty();
    }

    private static boolean tableExists() throws SQLException {
        try (Connection connection = DriverManager.getConnection(oracle.getJdbcUrl(), oracle.getUsername(), oracle.getPassword())) {
            ResultSet tableExists = connection.getMetaData().getTables(null, null, TEST_TABLE, null);
            if (tableExists.next()) {
                return true;
            }
        }
        return false;
    }


    private static List<Map<String, Object>> fetchRecordsFromOracle() throws SQLException, InterruptedException {
        List<Map<String, Object>> records = new ArrayList<>();
        long endTimeMillis = System.currentTimeMillis() + ORACLE_TIMEOUT;
        int sleepTimeMillis = 100;
        int maxSleepTimeMillis = 1000;

        while (System.currentTimeMillis() < endTimeMillis) {
            try (Connection connection = DriverManager.getConnection(oracle.getJdbcUrl(), oracle.getUsername(), oracle.getPassword());
                 Statement statement = connection.createStatement();
                 ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TEST_TABLE)) {

                ResultSetMetaData metaData = resultSet.getMetaData();
                int columnCount = metaData.getColumnCount();

                while (resultSet.next()) {
                    Map<String, Object> record = new HashMap<>();
                    for (int i = 1; i <= columnCount; i++) {
                        String columnName = metaData.getColumnName(i);
                        Object columnValue = resultSet.getObject(i);
                        record.put(columnName, columnValue);
                    }
                    records.add(record);
                }
            }

            if (!records.isEmpty()) {
                return records;
            }

            LOGGER.info("Waiting for {} ms before retrying to fetch records from Oracle", sleepTimeMillis);
            Thread.sleep(sleepTimeMillis);
            sleepTimeMillis = Math.min(sleepTimeMillis * 2, maxSleepTimeMillis);
        }
        return records;
    }


    private void runTableCreationCommand(final String command) throws SQLException {
        try (Connection connection = DriverManager.getConnection(oracle.getJdbcUrl(), oracle.getUsername(), oracle.getPassword());
             final Statement statement = connection.createStatement()) {
            statement.execute(command);
        }
    }

    private static class KafkaTopicCreator {
        public static void createTopic(final String bootstrapServers, final String topicName, final int partitions, final short replicationFactor) {
            final Properties config = new Properties();
            config.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

            try (AdminClient admin = AdminClient.create(config)) {
                final NewTopic newTopic = new NewTopic(topicName, partitions, replicationFactor);
                final Map<String, String> configs = new HashMap<>();
                configs.put("cleanup.policy", "compact");
                newTopic.configs(configs);
                admin.createTopics(Collections.singletonList(newTopic)).all().get();
            } catch (Exception e) {
                final String logs = kafka.getLogs();
                LOGGER.error("Failed to create topic {}. Kafka logs: {}", topicName, logs);
                throw new RuntimeException("Failed to create topic " + topicName, e);
            }
        }
    }

    @Test
    @Order(1)
    public void connectorPipeline_ValidProcessing_WhenTableExistsAndRecordsProduced() throws Exception {
        final String oracleTableCreationCommand = ConnectSchemaToTableCreationCommandGenerator.getTableCreationCommand(data, DbType.ORACLE, TEST_TABLE);
        runTableCreationCommand(oracleTableCreationCommand);
        if (!tableExists()) {
            fail("Table was not created");
        }

        final Map<String, Object> messageWithSchema = SchemaInjector.inject(data);

        final ObjectMapper objectMapper = new ObjectMapper();

        produceRecord(objectMapper.writeValueAsString(messageWithSchema));
        assertTrue(hasMessagesInTopic(), "No messages in topic");

        final List<Map<String, Object>> records = fetchRecordsFromOracle();
        if (records.size() == 0) {
            fail("Expected at least one row in the table");
        }
    }

    @Test
    @Order(2)
    @Disabled("Currently failing")
    public void connectorPipeline_InvalidProcessing_WhenTableExistsAndRecordsWithDifferentSchemasProduced() throws Exception {
        final Map<String, Object> messageWithSchema = SchemaInjector.inject(data);
        final Map<String, Object> messageWithDifferentSchema = SchemaInjector.inject(dataWithDifferentType);

        final ObjectMapper objectMapper = new ObjectMapper();
        produceRecord(objectMapper.writeValueAsString(messageWithSchema));

        assertTrue(hasMessagesInTopic(), "No messages in topic");

        Thread.sleep(5000);

        List<Map<String, Object>> records = fetchRecordsFromOracle();
        if (records.size() == 0) {
            fail("Expected at least one row in the table");
        }

        produceRecord(objectMapper.writeValueAsString(messageWithDifferentSchema));
        assertTrue(hasMessagesInTopic(), "No messages in topic");

        Thread.sleep(5000);
        records = fetchRecordsFromOracle();
        System.out.println(records);
        if (records.size() != 1) {
            fail("Expected for Oracle to fail.");
        }
    }

}
